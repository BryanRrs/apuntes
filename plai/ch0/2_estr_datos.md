# Estructuras de datos
## Par
La estructura par, une dos valores
```scheme
> (cons 1 2)
'(1 . 2)
```
Los pares tienen las funciones *car* y *cdr* que retornan el primer y segundo
elemento respectivamente
```scheme
> (car (cons 1 2))
1
> (cdr (cons 1 2))
2
```

## Lista
Se definen con la funcion *list*
```scheme
> (define l (list 1 2 3 4))
> l
'(1 2 3 4 5)
```
Es posible concatenarlas con *append*
```scheme
>(append (list 1 2 3) (list 4 5))
'(1 2 3 4 5)
```
Como una lista es en realidad una sucesion de pares encadenados, se puede
utilizar *car* y *cdr*, aunque existen funciones con nombres más amigables
como *first*, *rest*
```scheme
> (car l)
1
> (first l)
1
> (cdr l)
'(2 3 4 5)
> (rest l)
'(2 3 4 5)
> (second l)
2
> (fourth l)
4
```
Las listas aparecen con una comilla ', la que se utiliza para símbolos, lo que en el
caso de las listas quiere decir que no se evalue este elemento, que se tome como un dato
Es distinot definir listas con esta comilla que con list:
```racket
> (list 1 2 (+ 1 2))
'(1 2 3)
>'(1 2 (+ 1 2))
'(1 2 (+ 1 2))
```

## Vectores
Un vector es casi como un arreglo en C. La diferencia es que el acceso es seguro:
Si el indice esta fuera del vector, se lanza un error. La diferencia entre vector
y lista es que en el primero el acceso a un valor tiene costo constante.
```scheme
>(define v (vector 1 2 3))
>(vector-ref v 2)
3
```
Además, los vectores son por lo general mutables:
```scheme
>(vector set! v 2 "hola")
>v
'#(1 2 "hola")
```
Al igual que las listas, se pueden definir vectores de con un simbolo, en este caso #,
el cual crea un vector inmutable
```scheme
>(define v2 #(1 2 3))
```
Es posible además convertire vectores en listas y vice-versa:
```scheme
>(vector->list v2)
'(1 2 3)
>(list->vector '(1 2 3))
'#(1 2 3)
```
