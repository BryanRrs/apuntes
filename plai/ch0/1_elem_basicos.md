# Elementos básicos
## Tipos primitivos
- Números
```scheme
1
-3
4.02
6.02e+23
1+2i
4/3
```
- Booleans
```scheme
#t
#f
```
- Strings
```scheme
"hola"
"hola \"mundo\" feliz"
"λx:(μα.α→α).xx"
```
- Símbolos
```scheme
'hola
```
Los Símbolos son valores atómicos, por lo que comparar es barato a diferencia
de los string.

- Otros Tipos
Caracteres, bytes, byte strings, void, undefined, pairs, cajas, etc

## Usar funciones predefinidas
scheme es un lenguaje con sintaxis prefijada con paréntesis. El primer elemento
después del parentesis es la función a aplicar y todo el resto hasta el paréntesis 
de cierre son los argumentos, los que se separan por espacios.

Ej:
```scheme
> (+ 1 (- 3 4))
0
> (sqrt -1)
0+1i
> (or (< 5 4)
      (equal? 1 (- 6 5)))
#t
> (and (not (zero? 10))
        (+ 1 2 3))
6
```
En scheme, todo lo que no es #f es #t

- Manipulacion de strings
```scheme
> (string-append "ho" "la")
"hola"
> (string-lenght "hola mundo")
10
> (printf "hola~n")
hola
> (printf "hola ~a ~s~n" "mundo" "feliz")
hola mundo "feliz"
```

scheme es un lenguaje seguro (no segmentation faults) y genera
errores al invocar funciones con tipos erroneos.
```scheme
> (+ 1 "hola")
+: contract violation
....
```
Los errores se informan en runtime.

## Condicionales
```scheme
> (if (> 4 10)
      "hola"
      "chao")
"chao"
> (if (> 2 3)
      (printf "hola")
      (if (> 6 5)
          (printf "chao")
          #f))
chao
```

En lugar de if anidados se puede utilizar *cond*

```scheme
> (cond [(> 2 3) (printf "hola")]
        [(> 6 5) (printf "chao")]
        [else #f])
chao
```
## Definir identificadores
Se pueden definir identificadores tal que queden globalmente disponibles.
```scheme
(define MAX 100)
```
Al correrlo, el identificador MAX queda enlazado a 100
```scheme
> MAX
100
> (< 25 MAX)
#t
```
La sintaxis de identificadors de scheme es bastante liberal. Solo los parentesis
y los caracteres ,.;’‘|\ son especiales y las secuencias que forman los numeros
```scheme
> (define !λ=>-α/β?☺ "weird")
> !λ=>-α/β?☺
"weird"
```
Se pueden introducir identificadores locales con let
```scheme
>(let ([x 3]
       [y 2])
    (+ x y))
5
>x
x: undefined
```
## Definir funciones
Se pueden definir funciones

```scheme
(define (double x)
    (+ x x))
> (double 2)
4
```

