# Programar con Funciones
Antes de comenzar, hay que destacar que en Scheme las funciones son valores, o 
como se conoce en ciencias de la computación, Scheme posee [*first-class functions*](https://en.wikipedia.org/wiki/First-class_function)
## Funciones parametrizadas por funciones
Dado que las funcinoes son valores, una función puede recibir como parametro otra
función. Un caso típico es la función map, la que recibe una función y una lista
y aplica esa función a todos los elementos de la lista:
```scheme
>(define my-list '(1 2 3))
>(map add1 my-list)
'(2 3 4)
```
## Funciones anónimas
En scheme se puede utilizar la forma sintáctica *lambda* para definir funciones
anónimas.
```scheme
> (lambda (x) (+ x 2))
#<procedure>
```
Estas también son valores, por lo que es posible utilizarlas como parametro
```scheme
>(map (lambda (x) (+ x 2)) '(1 2 3))
'(3 4 5)
```
Es más, la forma de definir funciones antes, no es más que azúcar sintáctico 
para definir funciones anonimas con un identificador (ya no son anónimas!)
```scheme
(define (foo x) x) => (define foo (lambda (x) x))
```
## Funciones que producen funciones
Poder crear funciones anónimas permite definir funciones que crean otras funciones,
por ejemplo creemos una funcion que retorna otra función que sume n al valore que
reciba
```scheme
(define (addn n)
    (lambda (x)
        (+ x n)))
(define add10 (addn 10))
>(add10 12)
22
```

