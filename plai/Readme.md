# Programming Languages: Application and Interpretation (plai)
Apuntes "españolizados" del libro [plai](http://cs.brown.edu/~sk/Publications/Books/ProgLangs/2007-04-26/plai-2007-04-26.pdf), complementados con 
las clases del curso "Lenguajes de programación" de la Universidad de Chile.
