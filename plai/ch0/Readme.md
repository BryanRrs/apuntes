# C0: PrePLAI
Este mini-curso tiene como objetivo entregarle las nociones básicas para programar en Scheme siguiendo buenas prácticas de programación funcional. 
Este capítulo es una adaptación de la introducción del profesor [Éric Tanter](https://users.dcc.uchile.cl/~etanter/preplai/)
Se usa el lenguaje Racket, un super Scheme con variadas y poderosas librerías, y su ambiente de desarrollo, DrScheme.

- [Conceptos Básicos](1_elem_basicos.md)
- [Estructuras de datos](2_estr_datos.md)
- [Programando con funciones](3_prog_funciones.md)


